//1. What directive is used by Node.js in loading the modules it needs?
	Import Directive

//2. What is Node.js module contains a method for server creation?
	http

//3. What is the method of the http object responsible for creating a server using Node.js?
	createServer

//4. What method of the response object allows us to get status codes and content types?
	writeHead

//5. Where will console.log() output its contents when run in Node.js?
	command line

//6. What property of the request objects contains the address's endpoint?
	
	.url
